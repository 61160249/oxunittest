package test;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {

    public Scanner sc = new Scanner(System.in);
    public Table table;
    public int row;
    public int col;
    Player o = null;
    Player x = null;

    public Game() {
        this.o = new Player('o');
        this.x = new Player('x');
    }

    public void newGame() {
        if (getRandomNumber(1, 100) % 2 == 0) {
            this.table = new Table(o, x);
        } else {
            this.table = new Table(x, o);
        }
    }

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public void showWelcome() {
        System.out.println("Welcome to XOgame v.4");
    }

    public void showTable() {
    	System.out.println("=====================");
        char[][] data = this.table.getData();
        for (int row = 0; row < data.length; row++) {
         
            for (int col = 0; col < data.length; col++) {
                System.out.print(data[row][col]+" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void showTurn() {
        System.out.println("Turn " + table.getCurrentPlayer().getName());
    }

    public void inputRowCol() {
        while (true) {
            this.input();
            try {
                if (this.table.setRowCol(row, col)) {
                    return;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Please input in table");
            }

        }
    }

    public boolean inputContinue() {
        while (true) {
        	System.out.println("=====================");
            System.out.print("Continue? (y/n) : ");
            char input = sc.next().charAt(0);
            if (input == 'n') {
                return false;
            }
            if (input == 'y') {
                return true;
            }
        }
    }

    public void input() {
        while (true) {
            try {
                System.out.print("Please input Row Col : ");
                this.row = sc.nextInt() ;
                this.col = sc.nextInt() ;
                return;
            } catch (InputMismatchException e) {
                sc.next();
                System.out.println("Please in put number 1-3!!!");
            }
        }

    }

    public void showBye() {
    	System.out.println("=====================");
        System.out.println("Bye!!!");
    }

    public void showWin() {
    	System.out.println("=====================");
        System.out.println(table.getWinner().getName() + " Win!!!");
    }

    public void showDraw() {
    	System.out.println("=====================");
        System.out.println("Draw!!!");
    }

    public void run() {
        this.showWelcome();

        while (true) {
            runOnce();
            if (!inputContinue()) {
                break;
            }
        }
        showBye();

    }

    public void runOnce() {
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()) {
                this.showResult();
                this.showStat();
                return;

            }

            table.switchPlayer();

        }

    }

    public void showResult() {
        if (table.getWinner() != null) {
            showWin();
        } else {
            showDraw();
        }
    }

    public void showStat() {
        System.out.println(o.getName() + "--> win,lose,draw " + o.getWin() + ", " + o.getLose() + ", " + o.getDraw());
        System.out.println(x.getName() + "--> win,lose,draw " + x.getWin() + ", " + x.getLose() + ", " + x.getDraw());
    }
}
