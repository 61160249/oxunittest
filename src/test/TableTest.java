package test;
import static org.junit.jupiter.api.Assertions.*;
import Test.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import Test.Table;


class TableTest {

	public TableTest() {
		
    }

	
	public void TestRow1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());
    }
     public void TestCol1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());
    }
     public void testRow2Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(2,1);
        table.setRowCol(2,2);
        table.setRowCol(2,3);
        assertEquals(true,table.checkWin());
    }
     public void testCol2Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,2);
        table.setRowCol(2,2);
        table.setRowCol(3,2);
        assertEquals(true,table.checkWin());
    }
     public void testRow3Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(3,1);
        table.setRowCol(3,2);
        table.setRowCol(3,3);
        assertEquals(true,table.checkWin());
    }
     public void testCol3Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(1,3);
        table.setRowCol(2,3);
        table.setRowCol(3,3);
        assertEquals(true,table.checkWin());
    }
     
     public void testX1Win(){
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(1,1);
         table.setRowCol(2,2);
         table.setRowCol(3,3);
         assertEquals(true,table.checkWin());
     }
      public void testX2Win(){
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(1,3);
         table.setRowCol(2,2);
         table.setRowCol(3,1);
         assertEquals(true,table.checkWin());
     }
     
      
      public void testX1Lose(){
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(1,3);
         table.setRowCol(1,2);
         table.setRowCol(3,1);
         assertEquals(false,table.checkWin());
        }
      
       public void testSwitchPlayer(){
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.switchPlayer();
         assertEquals('x',table.getCurrentPlayer().getName());
        }
}
